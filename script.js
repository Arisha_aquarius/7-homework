// Теоретичні питання
//1.Опишіть своїми словами як працює метод forEach.
// Перебирает массив и не трансформирует его
// 2.Як очистити масив?
// const array = [1,2,3,]
// array.length = 0;
// Як можна перевірити, що та чи інша змінна є масивом?
// typeof

//Технічні вимоги:
// Написати функцію filterBy(), яка прийматиме 2 аргументи.
//  Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, 
//тип яких був переданий другим аргументом.
// Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].




function filterBy(arr,dataType) {
    return arr.filter(item => typeof item !== dataType)
}

const inputArray = ['hello', 'world', 23, '23', null];
const dataTypeToFilter = 'string';

const filteredArray = filterBy(inputArray, dataTypeToFilter);
console.log(filteredArray);